extends MarginContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var my_stats = []
onready var character_Name_Label = get_node("HBoxContainer/Character Stats/Characterstats/Characterstats/Character_name")
onready var character_Leben_Label = get_node("HBoxContainer/Character Stats/Characterstats/Characterstats/Leben")
var skill1_name = "default_hit_init"
var skill2_name = "default_hit_init"
var skill3_name = "default_hit_init"
var skill4_name = "default_hit_init"
var selected_skill_number = 0

#SkillIcons
var volt_strike = preload("res://Assets/GUI/Skill_Icon/if762.png")
var default_hit = preload("res://Assets/GUI/Skill_Icon/Icon1.png")
var pocket_sand = preload("res://Assets/GUI/Skill_Icon/32_6.png")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func set_picture(button,skill_name):
	if skill_name == "default_hit":
		button.set_normal_texture(default_hit)
	elif skill_name == "pocket_sand":
		button.set_normal_texture(pocket_sand)
	elif skill_name == "default_hit_init":
		pass
		
func set_skills_at_start(skill1,skill2,skill3,skill4):
	skill1_name = skill1
	skill2_name = skill2
	skill3_name = skill3
	skill4_name = skill4
	
	var skill1_button = get_node("HBoxContainer/Character Stats/Skills/Skill1")
	var skill2_button = get_node("HBoxContainer/Character Stats/Skills/Skill2")
	var skill3_button = get_node("HBoxContainer/Character Stats/Skills/Skill3")
	var skill4_button = get_node("HBoxContainer/Character Stats/Skills/Skill4")
	
	set_picture(skill1_button,skill1_name)
	set_picture(skill2_button,skill2_name)
	set_picture(skill3_button,skill3_name)
	set_picture(skill4_button,skill4_name)
		
	
func select_skill(number):
	var _skill_selection = get_node("HBoxContainer/SkillStats")
	_skill_selection.visible = true
	selected_skill_number = number
	
	
func set_skill(name):
	var number = selected_skill_number
	if number == 1:
		skill1_name = name
	if number == 2:
		skill2_name = name
	if number ==3:
		skill3_name = name
	if number ==4:
		skill4_name = name
	print("skill gesetzt zu: "+str(name))
	var GUI = get_parent()
	GUI.set_skill(skill1_name,1)
	GUI.set_skill(skill2_name,2)
	GUI.set_skill(skill3_name,3)
	GUI.set_skill(skill4_name,4)
	
	set_skills_at_start(skill1_name,skill2_name,skill3_name,skill4_name)
		
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func build_characterstats(Player,Game):
	Game.get_node("SummoningPortal").get_Demon_stats()
	var game_stats_demon = Game.get_node("SummoningPortal").killed_Demons2
	print(str(game_stats_demon))
	
	my_stats = []
	my_stats.append("maxLeben: "+str(Player.max_health))
	my_stats.append("curLeben: "+str(Player.currentHealth))
	my_stats.append("Lebensreg.: "+str(Player.player_life_reg)+"/s")
	my_stats.append("")
	my_stats.append("maxMana.: "+str(Player.max_mana))
	my_stats.append("curMana.: "+str(Player.current_Mana))
	my_stats.append("Manareg.: "+str(Player.player_mana_reg)+"/s")
	my_stats.append("")
	my_stats.append("Level: "+str(Player.current_level))
	my_stats.append("curXP: "+str(Player.current_XP))
	my_stats.append("XPtonextLVL: "+str(Player.XP_next_lvl))
	
func get_all_characterstats():
	print("hab sie")
	var Game = get_parent().get_parent().get_parent()
	var Player = Game.get_node("Player")
	build_characterstats(Player,Game)
	print(str(Player.max_health))
	var alle_stats = ""
	for a in my_stats:
		alle_stats = alle_stats+"\n"+a
	character_Name_Label.set_text(str(Player.player_name))
	character_Leben_Label.set_text(alle_stats)
	
func _on_SchlieenButton_pressed():
	#print("test")
	get_tree().paused = false
	queue_free()


func _on_Skill1_pressed():
	select_skill(1)
	print("was")

func _on_Skill2_pressed():
	select_skill(2)


func _on_Skill3_pressed():
	select_skill(3)

func _on_Skill4_pressed():
	select_skill(4)
	
func _on_Pocket_Sand_pressed():
	set_skill("pocket_sand")


