extends MarginContainer

onready var healthBar = $horizontalLayers/Bars/HealthBar
onready var manaBar = $horizontalLayers/Bars/ManaBar
onready var XPBar = $horizontalLayers/Bars/XPBar


var escMenu = preload("res://Scenes/GUI/Escape_Menu/EscMenu.tscn")
var characterMenu = preload("res://Scenes/GUI/Character_Stats/CharacterStats.tscn")
var characterMenu2 = preload("res://Scenes/GUI/Character_Stats/CharacterStatsWithSkillSelection.tscn")
var victory = preload("res://Scenes/GUI/Mission_End_Screens/Victory_Screen.tscn")
var dead = preload("res://Scenes/GUI/Mission_End_Screens/Dead.tscn")

var pocket_sand = preload("res://Scenes/Players/Pocket_Sand.tscn")


onready var skill1 = $"horizontalLayers/BottomLayer/Skills/NinePatchRect/Distance Groups/Skills/Skill1"
onready var skill2 = $"horizontalLayers/BottomLayer/Skills/NinePatchRect/Distance Groups/Skills/Skill2"
onready var skill3 = $"horizontalLayers/BottomLayer/Skills/NinePatchRect/Distance Groups/Skills/Skill3"
onready var skill4 = $"horizontalLayers/BottomLayer/Skills/NinePatchRect/Distance Groups/Skills/Skill4"

var skill1_cooldown_time = 2000
var skill1_current_cooldown = 0
var skill2_cooldown_time = 2000
var skill2_current_cooldown = 0
var skill3_cooldown_time = 2000
var skill3_current_cooldown = 0
var skill4_cooldown_time = 2000
var skill4_current_cooldown = 0

func _ready():
	pass

func set_skill(name, number):
	if number == 1:
		skill1.set_my_name(name)
	if number == 2:
		skill2.set_my_name(name)
	if number ==3:
		skill3.set_my_name(name)
	if number ==4:
		skill4.set_my_name(name)
	
func set_quest(text):
	var quest_label = get_node("horizontalLayers/Quest/QuestText/NinePatchRect/Label")
	quest_label.set_text(str(text))
	
func dead_player():
	var erstellteCharacter = dead.instance()
	add_child(erstellteCharacter)
	get_tree().paused = true
	
func updateProgressBar(currentAmount, maxAmount, typeAsString):
	if typeAsString == "Mana":
		print("Mana wurde geupdated")
		print (currentAmount)
		print (maxAmount)
		manaBar.updateProgressBar(currentAmount, maxAmount)
	elif typeAsString == "Health":
		healthBar.updateProgressBar(currentAmount, maxAmount)
	elif typeAsString == "XP":
		XPBar.updateProgressBar(currentAmount, maxAmount)
	else:
		print ("Der Befehl",typeAsString, "Ist unbekannt")


func _on_Player_health_update(currentAmount : int, new_max_amount :int):
	healthBar.updateProgressBar(currentAmount, new_max_amount)
	pass # Replace with function body.


func _on_Player_XP_update(currentAmount : int, new_max_amount :int):
	XPBar.updateProgressBar(currentAmount, new_max_amount)
	pass # Replace with function body.


func _on_Player_mana_update(currentAmount : int, new_max_amount :int):
	manaBar.updateProgressBar(currentAmount, new_max_amount)
	pass # Replace with function body.


func _on_Character_pressed():
	print("Character Button pressed")
	var erstellteCharacter = characterMenu2.instance()
	var skill1 = get_node("horizontalLayers/BottomLayer/Skills/NinePatchRect/Distance Groups/Skills/Skills/Skill1").skill_name
	var skill2 = get_node("horizontalLayers/BottomLayer/Skills/NinePatchRect/Distance Groups/Skills/Skills/Skill2").skill_name
	var skill3 = get_node("horizontalLayers/BottomLayer/Skills/NinePatchRect/Distance Groups/Skills/Skills/Skill3").skill_name
	var skill4 = get_node("horizontalLayers/BottomLayer/Skills/NinePatchRect/Distance Groups/Skills/Skills/Skill4").skill_name
	 
	erstellteCharacter.set_skills_at_start(skill1,skill2,skill3,skill4)
	add_child(erstellteCharacter)
	get_tree().paused = true
	erstellteCharacter.get_all_characterstats()

func _on_Inventar_button_up():
	print("Inventar Button pressed")
	pass # Replace with function body.

func _on_Hauptmen_pressed():
	print("Hauptmenü Button pressed")
	var erstellteMenu = escMenu.instance()
	add_child(erstellteMenu)
	get_tree().paused = true
	
func reward_player():
	var item = "XP"
	var amount = 100
	var GAME = get_parent().get_parent()
	var player = GAME.get_node("Player")
	player.give_item(item,amount)
	
func on_victory(anzahl_demonen):
	print("Hauptmenü Button pressed")
	var erstellteMenu = victory.instance()
	erstellteMenu.set_demons(anzahl_demonen)
	reward_player()
	add_child(erstellteMenu)
	get_tree().paused = true
	
func use_skill_1():
	pass
	skill1_current_cooldown = skill1_cooldown_time
	print("Skill1 Button pressed")
	var GAME = get_parent().get_parent()
	var player = GAME.get_node("Player")
	var erstellteMenu = pocket_sand.instance()
	player.add_child(erstellteMenu)
	erstellteMenu.play_skill("West")
	#get_tree().paused = true
	
	
func use_skill_2():
	skill2_current_cooldown = skill2_cooldown_time
	
func use_skill_3():
	skill3_current_cooldown = skill3_cooldown_time
	
func use_skill_4():
	skill4_current_cooldown = skill4_cooldown_time
	
func _process(delta):
	if skill1_current_cooldown >0:
		skill1_current_cooldown-=1
		var skill1_cooldown = get_node("horizontalLayers/BottomLayer/Skills/NinePatchRect/Distance Groups/Skills/Skills/Skill1/NinePatchRect/Cooldown")
		skill1_cooldown.set_text(str(skill1_current_cooldown))
		
	if skill2_current_cooldown >0:
		skill2_current_cooldown-=1
		var skill2_cooldown = get_node("horizontalLayers/BottomLayer/Skills/NinePatchRect/Distance Groups/Skills/Skills/Skill2/NinePatchRect/Cooldown")
		skill2_cooldown.set_text(str(skill2_current_cooldown))
		
	if skill3_current_cooldown >0:
		skill3_current_cooldown-=1
		var skill3_cooldown = get_node("horizontalLayers/BottomLayer/Skills/NinePatchRect/Distance Groups/Skills/Skills/Skill3/NinePatchRect/Cooldown")
		skill3_cooldown.set_text(str(skill3_current_cooldown))
		
	if skill4_current_cooldown >0:
		skill4_current_cooldown-=1
		var skill4_cooldown = get_node("horizontalLayers/BottomLayer/Skills/NinePatchRect/Distance Groups/Skills/Skills/Skill4/NinePatchRect/Cooldown")
		skill4_cooldown.set_text(str(skill4_current_cooldown))
		
	if Input.is_action_just_pressed("ui_skill1"):
		use_skill_1()
	if Input.is_action_just_pressed("ui_skill2"):
		use_skill_2()
	if Input.is_action_just_pressed("ui_skill3"):
		use_skill_3()
	if Input.is_action_just_pressed("ui_skill4"):
		use_skill_4()
