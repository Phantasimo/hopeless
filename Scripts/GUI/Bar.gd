extends HBoxContainer

onready var progressbar = $TextureProgress
onready var number = $Count/Background/Number




func updateProgressBar(newAmount, newMaxAmount):
	number.text = str(newAmount) + "/" + str(newMaxAmount)
	progressbar.max_value = newMaxAmount
	progressbar.value = newAmount
	
