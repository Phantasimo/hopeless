extends Control

onready var healthbar = $Healthbar
onready var text = $Healthbar/Label
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var player = get_parent().get_node("Player")

func _ready():
	print (healthbar)
	
# Called when the node enters the scene tree for the first time.
func updateHealth(value, max_value):
	healthbar.max_value = max_value
	healthbar.value = value

	
	text.text = str(healthbar.value) + "/" + str(healthbar.max_value)

func _stay_by_player():
	rect_position.x = player.position.x -50
	rect_position.y = player.position.y -50

func _physics_process(delta):
	_stay_by_player()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
