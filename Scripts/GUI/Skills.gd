extends MarginContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var skill_name = "default_hit"

#SkillIcons
var volt_strike = preload("res://Assets/GUI/Skill_Icon/if762.png")
var default_hit = preload("res://Assets/GUI/Skill_Icon/Icon1.png")
var pocket_sand = preload("res://Assets/GUI/Skill_Icon/32_6.png")
var normal_attack = preload("res://Assets/GUI/Skill_Icon/if230.png")
func set_my_name(name):
	skill_name = name
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var my_picture = get_node("NinePatchRect/TextureRect")
	if skill_name == "default_hit":
		my_picture.set_texture(default_hit)
	elif skill_name == "pocket_sand":
		my_picture.set_texture(pocket_sand)
	elif skill_name == "default_hit_init":
		pass
	elif skill_name == "normal_attack":
		my_picture.set_texture(normal_attack)
		
