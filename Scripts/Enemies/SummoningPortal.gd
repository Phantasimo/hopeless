extends AnimatedSprite


var scene = preload("res://Scenes/Enemies/Demon.tscn")


var demonSummonTime = 20 #In Frames zurzeit
var currentTime = 0
var summonedDemons = 0
var nextHoundred = 100
var currentHoundred = 0


var killed_Demons2 = 0
var demons_to_kill = 20
func get_Demon_stats():
	var killed_Demons = 0
	var all_demons = get_tree().get_nodes_in_group("DEMONS")
	for a in all_demons:
		if a.is_dead == true:
			killed_Demons+=1
			

func victory():
	var UI = get_parent().get_node("CanvasLayer/GUI")
	UI.on_victory(demons_to_kill)
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func one_dead_demon():
	killed_Demons2+=1
	var text = "Quest\n\n Töte Dämonen\n\n "+str(killed_Demons2)+"/"+str(demons_to_kill)
	var UI = get_parent().get_node("CanvasLayer/GUI")
	UI.set_quest(text)
	if killed_Demons2 == demons_to_kill:
		print("Victory")
		victory()
		
	
func summonDemon():
	#only spawn the ammount of demons you need to finish the Quest
	if demons_to_kill > summonedDemons:
		var demon = scene.instance()
		add_child(demon)
		demon.add_to_group("DEMONS")
		summonedDemons +=1
		currentHoundred +=1
		if currentHoundred >= nextHoundred:
			currentHoundred = 0
			print(summonedDemons)

func _physics_process(delta):
	currentTime += 1
	if currentTime >= demonSummonTime:
		summonDemon()
		currentTime = 0
