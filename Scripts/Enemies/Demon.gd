extends KinematicBody2D

var path = PoolVector2Array()

signal sendXPValue


var movement = Vector2 (0,0)
var max_health = 100
var current_health = 20
var xp_amount = 10
var bodys_of_interest = []
var attack = 10

var max_speed = 100
var current_moving_direction = Vector2 (0,0)

var power_demons = 0.05
var push_closeness = 50

var attack_distance_to_player = 50

var is_dead = false

onready var animator = $AnimatedSprite
onready var navigation2D = get_parent().get_parent().get_node("Navigation2D")
onready var tile_map = navigation2D.get_node("Hintergrund")
onready var character = get_parent().get_parent().get_node("Player")

onready var attack_area_east = $AttackEast/CollisionEast
onready var attack_area_west = $AttackWest/CollisionWest
onready var attack_area_north = $AttackNorth/CollisionNorth
onready var attack_area_south = $AttackSouth/CollisionSouth

var look_direction = "S"

var look_in_a_direction_for_some_frames = 6
var current_time_of_looking_in_a_direction = 0

var attack_speed = 50
var attack_allowed = true

var current_path_calculation_frame = 0
var next_path_calculation = 10
var follow_point = 1

# Called when the node enters the scene tree for the first time.
func _ready():

	calculate_Path()


func new_Movement(delta):
	current_path_calculation_frame += 1
	if current_path_calculation_frame >= next_path_calculation:
		calculate_Path()
		current_path_calculation_frame = 0
		if not path.size() == 0:
			next_path_calculation = path.size() * 5
			
	current_moving_direction = current_moving_direction + follow_Path2()
	walking_animation()
	
	if not bodys_of_interest.size() == 0:
		avoiding_demons()
	
	if (abs(current_moving_direction.x) + abs(current_moving_direction.y) > max_speed):
		var divisor = abs(current_moving_direction.x) + abs (current_moving_direction.y)
		current_moving_direction.x = current_moving_direction.x * max_speed / divisor
		current_moving_direction.y = current_moving_direction.y * max_speed / divisor
	
	var collision = move_and_collide(current_moving_direction * delta)
	return collision

func walking_animation():
	current_time_of_looking_in_a_direction += 1
	if current_time_of_looking_in_a_direction < look_in_a_direction_for_some_frames:
		return
	current_time_of_looking_in_a_direction = 0
	var type = "Walking_"
	var vector_length_one = current_moving_direction.normalized()
	
	if vector_length_one.x >  0.75:
		look_direction = "E"
	if vector_length_one.x < -0.75:
		look_direction = "W"
	if vector_length_one.y < -0.75:
		look_direction = "N"
	if vector_length_one.y > 0.75:
		look_direction = "S"
	animator.play(type + look_direction)
	
	
func avoiding_demons():
	var push_other_demons = Vector2 (0,0)
	for body in bodys_of_interest:
		var distance = self.global_position.distance_to(body.global_position)
		var push_demon = self.global_position - body.global_position
		if abs(push_demon.x) + abs(push_demon.y) == 0:
			return Vector2.ZERO
		push_demon = push_demon / (abs(push_demon.x) + abs(push_demon.y))
		if distance < push_closeness:
			push_demon.x *= max_speed  * 3
			push_demon.y *= max_speed  * 3
			push_other_demons.x += push_demon.x
			push_other_demons.y += push_demon.y
		else:
			var power = max_speed
			power = power /(0.1* (distance - (push_closeness - (1/0.1))))

			push_demon.x *= power * power_demons * 2
			push_demon.y *= power * power_demons * 2
			push_other_demons.x += push_demon.x
			push_other_demons.y += push_demon.y
			
	current_moving_direction += push_other_demons


func is_it_time_to_calculate_Path() -> bool:
	if current_path_calculation_frame >= next_path_calculation:
		current_path_calculation_frame = 0
		if not path.size() == 0:
			next_path_calculation = path.size() * 5
		return true
	return false

func calculate_Path():
	if is_it_time_to_calculate_Path():
		follow_point = 1
		path = navigation2D.get_simple_path(self.global_position, character.global_position)
		
func follow_Path2():
	if path.size() == 0:
		return Vector2.ZERO
	if follow_point >= path.size():
		return Vector2.ZERO
	var something = Vector2 (path[follow_point] - self.global_position)
	if something.length() <= 5:
		follow_point+=1
		if follow_point >= path.size():
			return Vector2.ZERO
		something = Vector2 (path[follow_point] - self.global_position)
	something = something.normalized()
	something.x *= max_speed
	something.y *= max_speed
	return something





func take_dmg(dmg):
	current_health -= dmg
	if current_health <= 0:
		character._get_XP(xp_amount)
		emit_signal("sendXPValue", xp_amount)
		$Hurtbox.queue_free()
		$CollisionShape2D.queue_free()
		is_dead = true
		animator.play("Death")

func _physics_process(delta):
	if is_dead:
		return
	var distance_to_player = self.global_position.distance_to(character.global_position)
	if distance_to_player > attack_distance_to_player:
		new_Movement(delta)
	else:
		if attack_allowed:
			animator.play("Attack_" + look_direction)
			enable_relevant_attack_area()
			attack_allowed = false
			yield(get_tree().create_timer(1.0), "timeout")
			disable_all_attack_fields()
			attack_allowed = true
			animator.frame = 0

func enable_relevant_attack_area():
	if look_direction == "S":
		attack_area_south.disabled = false
	if look_direction == "N":
		attack_area_north.disabled = false
	if look_direction == "W":
		attack_area_west.disabled = false
	if look_direction == "E":
		attack_area_east.disabled = false

func _on_AnimatedSprite_animation_finished():
	if animator.animation == "Death":
		queue_free()
		var summoning_portal = get_parent()
		#summoning_portal.one_dead_demon()
	if "Attack" in animator.animation:
		animator.frame = 0



func disable_all_attack_fields():
	attack_area_east.disabled = true
	attack_area_north.disabled = true
	attack_area_south.disabled = true
	attack_area_west.disabled = true
	pass

func _on_Area2D_dmg(dmg: int):
	take_dmg(dmg)
	pass # Replace with function body.


func _on_Hurtbox_area_entered(area):
	if area.name == "Hurtbox":
		bodys_of_interest.append(area)
	if "AttackArea" in area.name:
		take_dmg(10)

func _on_Hurtbox_area_exited(area):
	if area.name == "Hurtbox":
		bodys_of_interest.erase(area)


func _on_Hurtbox_body_entered(body):
	if body.name == "Player":
		if body.has_method("take_dmg"):
			pass
			#body.take_dmg(1)
	print (body)
	pass # Replace with function body.


func player_take_dmg(body):
	if body.name == "Player":
		if body.has_method("take_dmg"):
			body.take_dmg(attack)


func _on_AttackAreaEast_body_entered(body):
	player_take_dmg(body)
	pass # Replace with function body.


func _on_AttackAreaSouth_body_entered(body):
	player_take_dmg(body)
	pass # Replace with function body.


func _on_AttackAreaWest_body_entered(body):
	
	player_take_dmg(body)
	pass # Replace with function body.


func _on_AttackAreaNorth_body_entered(body):
	player_take_dmg(body)
	pass # Replace with function body.
